'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope','Phone',
  function($scope, Phone) {
    $scope.phones = Phone.query(function(phones) {
      //var tempPhoneDetailList = [];
      var count = 0;
    	for(var i = 0; i < $scope.phones.length; i++) {
	   Phone.get({phoneId: $scope.phones[i].id}, function(phone) {
		if (phone.sizeAndWeight.weight != "") {
		  $scope.phones[count].weight = parseFloat(phone.sizeAndWeight.weight.substring(0, phone.sizeAndWeight.weight.length-6));
		}
		else{
		  $scope.phones[count].weight = 0;
		}
		if (phone.storage.flash != "") {
		  $scope.phones[count].flash = parseFloat(phone.storage.flash.substring(0, phone.storage.flash.length-2));
		}
		else{
		  $scope.phones[count].flash = 0;
		}
		if (phone.storage.ram != "") {
		  $scope.phones[count].ram = parseFloat(phone.storage.ram.substring(0, phone.storage.ram.length-2));
		}
		else{
		  $scope.phones[count].ram = 0;
		}
		if (phone.display.screenSize != "") {
		  $scope.phones[count].screenSize = parseFloat(phone.display.screenSize.substring(0, phone.display.screenSize.length-7));
		}
		else{
		  $scope.phones[count].screenSize = 0;
		}
		count++;
	   });
    	}
    });
    $scope.orderProp = 'age';
    $scope.comparePhones = function(){
      if ($scope.phoneId1 == undefined || $scope.phoneId2 == undefined) {
	return;
      }
      var path = "/" + $scope.phoneId1 + "&" + $scope.phoneId2;
      window.location.href = window.location.href + path;
    }
  }]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);

phonecatControllers.controller('PhoneCompareCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone1 = Phone.get({phoneId: $routeParams.phoneId1}, function(phone) {
      checkForNotAvailables(phone);
    });
    $scope.phone2 = Phone.get({phoneId: $routeParams.phoneId2}, function(phone) {
      checkForNotAvailables(phone);
    });
    
    var checkForNotAvailables = function(phone) {
      var attributes = [];
      if (phone.availability[0] == "") {
	phone.availability[0] = "Not Available";
      }
      if (phone.battery.standByTime == "") {
	phone.battery.standByTime = "Not Available";
      }
      if (phone.additionalFeatures == "") {
	phone.additionalFeatures = "Not Available";
      }
      if (phone.battery.talkTime == "") {
	phone.battery.talkTime = "Not Available";
      }
      if (phone.hardware.audioJack == "") {
	phone.hardware.audioJack = "Not Available";
      }
      if (phone.connectivity.cell == "") {
	phone.connectivity.cell = "Not Available";
      }
      if (phone.storage.flash == "") {
	phone.storage.flash = "Not Available";
      }
      if (phone.storage.ram == "") {
	phone.storage.ram = "Not Available";
      }
      if (phone.camera.primary == "") {
	phone.camera.primary = "Not Available";
      }
      if (phone.camera.features[0] == "") {
	phone.camera.features[0] = "Not Available";
      }
      if (phone.android.ui == "") {
	phone.android.ui = "Not Available";
      }   
    }
    
    
  }]);
